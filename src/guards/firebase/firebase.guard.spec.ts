import { FirebaseAuthGuard } from './firebase.guard';
import { UnauthorizedException, ExecutionContext } from '@nestjs/common';
import * as admin from 'firebase-admin';

jest.mock('firebase-admin', () => ({
	auth: jest.fn().mockReturnThis(),
	verifyIdToken: jest.fn(),
}));

describe('FirebaseAuthGuard', () => {
	let guard: FirebaseAuthGuard;

	beforeEach(() => {
		guard = new FirebaseAuthGuard();
		(admin.auth().verifyIdToken as jest.Mock).mockReset();
	});

	it('should throw an UnauthorizedException if no authorization header is provided', async () => {
		const mockExecutionContext = {
			switchToHttp: () => ({
				getRequest: (): {headers: {}} => ({
					headers: {},
				}),
			}),
		} as ExecutionContext;

		await expect(guard.canActivate(mockExecutionContext)).rejects.toThrow(UnauthorizedException);
	});

	it('should successfully authenticate if the token is valid', async () => {
		const mockExecutionContext = {
			switchToHttp: () => ({
				getRequest: (): {headers: { authorization: string}} => ({
					headers: { authorization: 'Bearer valid_token' },
				}),
			}),
		} as ExecutionContext;

		(admin.auth().verifyIdToken as jest.Mock).mockResolvedValue({ uid: '1234' });

		const result = await guard.canActivate(mockExecutionContext);
		expect(result).toBe(true);
	});

	it('should throw an UnauthorizedException if token verification fails', async () => {
		const mockExecutionContext = {
			switchToHttp: () => ({
				getRequest: (): {headers: { authorization: string}} => ({
					headers: { authorization: 'Bearer invalid_token' },
				}),
			}),
		} as ExecutionContext;

		(admin.auth().verifyIdToken as jest.Mock).mockRejectedValue(new Error('Token verification failed'));

		await expect(guard.canActivate(mockExecutionContext)).rejects.toThrow(UnauthorizedException);
	});
});
