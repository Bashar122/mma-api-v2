import {
	Injectable,
	CanActivate,
	ExecutionContext,
	UnauthorizedException,
} from '@nestjs/common';
import * as admin from 'firebase-admin';

@Injectable()
export class FirebaseAuthGuard implements CanActivate {
	public async canActivate(context: ExecutionContext): Promise<boolean> {
		const request = context.switchToHttp().getRequest();
		console.log.apply('HEADERS: ', request.headers);
		if (!request.headers.authorization) {
			throw new UnauthorizedException('No authorization header.');
		}

		const token = request.headers.authorization.split(' ')[1];
		try {
			const decodedToken = await admin.auth().verifyIdToken(token);
			request.user = decodedToken; // Attach the user's information to the request
			return true;
		} catch (error) {
			throw new UnauthorizedException('Invalid token.');
		}
	}
}
