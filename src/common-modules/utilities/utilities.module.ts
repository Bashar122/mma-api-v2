import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { SecurityService } from './services/security.service';
import { ScoringService } from './services/scoring.service';
import { FightersHelperService } from './services/fighters-helper.service';
import { LoggerModule } from '../logger/logger.module';
import { TeamValidationService } from './services/team-validation.service';
import { FirestoreService } from './services/firestore.service';

@Module({
	controllers: [],
	exports: [
		FightersHelperService,
		ScoringService,
		SecurityService,
		TeamValidationService,
		FirestoreService,
	],
	imports: [
		LoggerModule,
		HttpModule,
	],
	providers: [
		FightersHelperService,
		ScoringService,
		SecurityService,
		TeamValidationService,
		FirestoreService,
	],
})
export class UtilitiesModule { }
