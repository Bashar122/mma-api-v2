import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { LoggerService } from 'src/common-modules/logger/logger.service';
import { v4 as uuidv4 } from 'uuid';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import * as cheerio from 'cheerio';
import { FirestoreService } from './firestore.service';

@Injectable()
export class FightersHelperService {
	constructor(
		private readonly loggerService: LoggerService,
		private readonly firestoreService: FirestoreService,
		private readonly httpService: HttpService
	) {}

	public getNewFighter(path: string): Observable<any> {
		const url = `http://www.sherdog.com${path}`;
		return this.httpService.get(url).pipe(
			map(response => this.extractFighterData(response.data, path)),
			catchError(error => {
				this.loggerService.error('Failed to retrieve fighter', error);
				return throwError(error);
			})
		);
	}

	private async extractFighterData(html: string, path): Promise<any> {
		const $ = cheerio.load(html);
		const fighter = {
			name: $('.bio_fighter span.fn').text(),
			nickname: $('.bio_fighter span.nickname').text().replace(/['"]+/g, ''),
			age: $('.bio_holder b').first().text().slice(5),
			birthday: $('.bio_holder span[itemprop="birthDate"]').text(),
			locality: $('.fighter-nationality span[itemprop="addressLocality"]').text(),
			nationality: $('.fighter-nationality strong[itemprop="nationality"]').text(),
			association: $('.bio_holder span[itemprop="name"]').text(),
			height: $('.bio_holder b[itemprop="height"]').text(),
			weight: $('.bio_holder b[itemprop="weight"]').text(),
			weight_class: $('.association-class a').last().text(),
			image_url: $('.profile-image-mobile.photo').attr('src'),
			wins: {
				total: 0, // Will be populated below
				knockouts: 0, // Will be populated below
				submissions: 0, // Will be populated below
				decisions: 0, // Will be populated below
				others: 0 // Will be populated below
			},
			losses: {
				total: 0, // Will be populated below
				knockouts: 0, // Will be populated below
				submissions: 0, // Will be populated below
				decisions: 0, // Will be populated below
				others: 0 // Will be populated below
			},
			no_contests: parseInt($('.winsloses-holder .loses .right_side .bio_graph .counter').text()) || 0,
			fights: [], // Will be populated below
			sherdogId: null, // Will be populated below
			id: uuidv4(),
		};

		// Populate the detailed properties here
		// Example for wins.total:
		fighter.wins.total = parseInt($('.wins .win span:nth-child(2)').text()) || 0;
		// Continue for others like fighter.wins.knockouts, fighter.losses.total, etc.

		// Extract fight history
		$('.module.fight_history tr:not(.table_head)').each(function () {
			const row = $(this);
			const fight = {
				result: row.find('td:nth-child(1) .final_result').text(),
				opponent_name: row.find('td:nth-child(2) a').text(),
				// Add more properties as needed
			};
			fighter.fights.push(fight);
		});

		// Extract and set the Sherdog ID from the path
		let fighterId = path.split('/').pop();
		fighter.sherdogId = fighterId;

		await this.firestoreService.addRecord('fighters', fighter);
		return fighter;
	}
}
