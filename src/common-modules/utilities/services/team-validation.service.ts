import { Injectable } from '@nestjs/common';
import { LoggerService } from 'src/common-modules/logger/logger.service';
import { FirestoreService } from './firestore.service';

export const winnersMap = new Map<number, string>([
	[3, 'draw'],
]);

export const methodsMap = new Map<number, string>([
	[1, 'submission'],
	[2, 'tko'],
	[3, 'decision'],
]);

export const roundsMap = new Map<number, string>([
	[1, '1'],
	[2, '2'],
	[3, '3'],
]);

@Injectable()
export class TeamValidationService {
	constructor(
		private readonly firestoreService: FirestoreService,
		private readonly loggerService: LoggerService,
	) { }

	public async validatePicks(event, teams): Promise<any> {
		const lineup = event.lineup;

		for (const [index, match] of lineup.entries()) {
			if (match.started) {
				for (const team of teams) {
					if (!team.picks[team.picks.length -1].picks[index].winner) {
						const consecutiveDraws = 4;
						let loopCount = 0;
						let winner = Math.floor(Math.random() * (4 - 1) + 1);

						if (winner == 3) {
							while (loopCount < consecutiveDraws) {
								winner = Math.floor(Math.random() * (4 - 1) + 1);
								const isDraw = winner === 3 ? true : false;
                                
								if (isDraw) {
									loopCount++;
								} else {
									break;
								}
							}
						}

						winnersMap.set(1, match.fight.fighter1.id);
						winnersMap.set(2, match.fight.fighter2.id);
						team.picks[team.picks.length -1].picks[index].winner = winnersMap.get(winner);
    
						const method = Math.floor(Math.random() * (4 - 1) + 1);
						team.picks[team.picks.length -1].picks[index].method = methodsMap.get(method);
    
						const round = Math.floor(Math.random() * (4 - 1) + 1);
						team.picks[team.picks.length -1].picks[index].round = roundsMap.get(round);

						try {
							await this.firestoreService.updateRecord('teams', team.id, team);
						} catch (ex) {
							this.loggerService.error(`Error updating ${team}`, ex);
							return { event, message: `Auto generating picks` };
						}
					}
				}
			}
		}
	}
}
