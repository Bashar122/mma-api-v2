import { Injectable } from '@nestjs/common';
import { compare, hash } from 'bcrypt';
import { v4 } from 'uuid';

/**
 * I don't believe this class is testable unless we figure out how to mock uuid and bcrypt functions.
 * This class was made specifically as an adapter so we could mock these function interactions in other classes
 *   and unit test the functionality that way.
 */
@Injectable()
export class SecurityService {
	public generateUUID(): string {
		return v4();
	}

	public async generateHash(password: string): Promise<string> {
		return await hash(password, 5);
	}

	public async compareHash(password: string, hash: string): Promise<boolean> {
		return await compare(password, hash);
	}
}