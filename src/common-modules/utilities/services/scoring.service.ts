import { Injectable } from '@nestjs/common';
import { ScoringTypes } from 'src/types/league.types';
import { FirestoreService } from './firestore.service';

@Injectable()
export class ScoringService {
	constructor(
		private readonly firestoreService: FirestoreService,
	) { }

	public scoreConfidence(result, pick): number {
		if (result.winner && result.winner === pick.winner) {
			return pick.confidence;
		}

		return 0;
	}

	public scoreMethod(result, pick): number {
		let matchPoints = 0;

		if (result.winner && result.winner === pick.winner) {
			if (pick.winner === 'draw') {
				return 400;
			}

			matchPoints += 200;
		} else {
			return 0;
		}

		if (result.method === pick.method) {
			matchPoints += 75;
		}

		if (pick.method !== 'decision' && result.round === pick.round) {
			matchPoints += 50;
		}

		for (const bonus of result.fight.bonus) {
			if (bonus.recipient === 'both' || bonus.recipient === pick.winner) {
				matchPoints += bonus.amount;
			}
		}

		return matchPoints;
	}

	public potentialScore(pick, fight): number {
		let matchPoints = 0;

		if (pick?.winner === 'draw') {
			return 400;
		}

		// maximium potential winner score
		matchPoints += 200;

		// maximium potential method score
		matchPoints += 75;

		// maximium potential round score
		if (pick?.method !== 'decision') {
			matchPoints += 50;
		}

		// maximium potential bonuses
		for (const bonus of fight.bonus) {
			if (bonus.recipient === 'both' || bonus.recipient === pick?.winner) {
				matchPoints += bonus.amount;
			}
		}

		// maximium potential points earned 
		return matchPoints;
	}

	public getBonusPts(fighterId: string, fight: any): any[] {
		let bonusPts = [];

		for (const bonus of fight.bonus) {
			if (bonus.recipient === fighterId || bonus.recipient === 'both') {
				bonusPts.push(bonus);
			}
		}

		return bonusPts;
	}

	public getRecord(team, opponents): any {
		team.record.losses = 0;
		team.record.wins = 0;
		team.record.ties = 0;
		
		for (const [weekIndex, pick] of team.picks.entries()) {
			for (const opponent of opponents) {
				if (pick.opponent === opponent.id) {
					if (pick.totalPoints > opponent.picks[weekIndex].totalPoints) {
						team.record.wins += 1;
					} else if (pick.totalPoints < opponent.picks[weekIndex].totalPoints) {
						team.record.losses += 1;
					} else if (pick.totalPoints === opponent.picks[weekIndex].totalPoints) {
						team.record.ties += 1;
					}

					continue;
				}
			}
		}

		return team;
	}

	public async scoreMatchUps(teams, currentWeek): Promise<any> {
		const opponents: Map<string, any> = new Map();

		for (const team of teams) {
			opponents.set(team.id, team);
		}

		
		const eventId = teams[0].picks[currentWeek]?.event;
		const event: any = await this.firestoreService.getRecord('events', eventId);
		const isLastMatchFinished: boolean = event?.lineup[0].winner ? true : false;
		
		for (const team of teams) {
			let teamTotal: number = 0;
			let opponentTotal: number = 0;
			const opponent = opponents.get(team.picks[currentWeek]?.opponent);
			
			if (event?.scoringType === ScoringTypes.METHOD) {
				for (const [index, result] of event.lineup.entries()) {
					teamTotal += this.scoreMethod(result, team.picks[currentWeek]?.picks[index]);
					opponentTotal += this.scoreMethod(result, opponent.picks[currentWeek]?.picks[index]);
				}
			} else if (event?.scoringType === ScoringTypes.CONFIDENCE) {
				for (const [index, result] of event.lineup.entries()) {
					teamTotal += this.scoreConfidence(result, team.picks[currentWeek]?.picks[index]);
					opponentTotal += this.scoreConfidence(result, opponent.picks[currentWeek]?.picks[index]);
				}
			}
			
			if (isLastMatchFinished) {
				event.completed = true;
				await this.firestoreService.updateRecord('events', event.id, event);
				await this.updateRecords(team, teamTotal, opponentTotal, event?.scoringType);
				continue;
			}
			
			await this.checkIfScoreChanged(team, teamTotal, currentWeek);
			await this.checkIfScoreChanged(opponent, opponentTotal, currentWeek);
		}
		
		return teams;
	}

	private async updateRecords(
		team: any,
		teamTotal: number,
		opponentTotal: number,
		scoringType: string,
	): Promise<void> {
		const currentWeek: number = team.picks.length - 1;

		if (currentWeek === -1) {
			return;
		}

		if (teamTotal > opponentTotal) {
			team.picks[currentWeek].result = 'win';
			team.record.wins += 1;
		} else if (teamTotal < opponentTotal) {
			team.picks[currentWeek].result = 'loss';
			team.record.losses += 1;
		} else if (teamTotal === opponentTotal) {
			team.picks[currentWeek].result = 'draw';
			team.record.ties += 1;
		}
		
		team.picks[currentWeek].totalPoints = teamTotal;

		if (scoringType === ScoringTypes.METHOD) {
			team.totalPoints += teamTotal;
			team.totalPointsAgainst += opponentTotal;
		} 
		
		await this.firestoreService.updateRecord('teams', team.id, team);
	}

	private async checkIfScoreChanged(team, newTotalPoints, currentWeek): Promise<void> {
		if (currentWeek === -1) {
			return;
		}

		if (team.picks[currentWeek]?.totalPoints != newTotalPoints) {
			team.picks[currentWeek].totalPoints = newTotalPoints;
			
			await this.firestoreService.updateRecord('teams', team.id, team);
		}
	}
}
