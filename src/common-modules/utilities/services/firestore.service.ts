import { Injectable, Inject } from '@nestjs/common';
import * as admin from 'firebase-admin';

@Injectable()
export class FirestoreService {
	private firestore: admin.firestore.Firestore;

	constructor(
		@Inject('FIREBASE_INSTANCE') private readonly firebase: admin.app.App,
	) {
		this.firestore = admin.firestore();
	}

	public async addRecord(collection: string, data: any): Promise<string> {
		// const documentRef = await this.firestore.collection(collection).add(data);
		if (!data.id) {
			return null;
		}

		const documentRef = await this.firestore.collection(collection).doc(data.id).set(data)
			.then(() => {
				return `Document successfully written with ID: ${data.id}`;
			})
			.catch((error) => {
				return `Error writing document: ${error}`;
			});
		return documentRef;
	}

	public async getRecord(collection: string, documentId: string = null): Promise<any> {
		const documentRef = this.firestore.collection(collection).doc(documentId);
		const documentSnapshot = await documentRef.get();

		if (documentSnapshot.exists) {
			return { id: documentSnapshot.id, ...documentSnapshot.data() };
		} else {
			throw new Error('Document not found');
		}
	}

	public async getAllDocuments(collection): Promise<any[]> {
		const collectionRef = this.firestore.collection(collection);
		const snapshot = await collectionRef.get();
	
		const documents = [];
		snapshot.forEach(doc => {
			documents.push({ id: doc.id, ...doc.data() });
		});
	
		return documents;
	}

	public async getDocumentByField(collection, fieldName, value): Promise<any> {
		const collectionRef = this.firestore.collection(collection);
		const snapshot = await collectionRef.where(fieldName, '==', value).get();
	
		if (snapshot.empty) {
			console.log('No matching documents.');
			return null;
		}
	
		const documents = [];
		snapshot.forEach(doc => {
			documents.push({ id: doc.id, ...doc.data() });
		});
	
		// Assuming you're looking for one document, return the first one
		// If there could be multiple, you might return `documents` instead
		return documents[0];
	}

	public async updateRecord(collection: string, id: string, data): Promise<string> {
		const docRef = this.firestore.collection(collection).doc(id);

		const result = docRef.set(data, { merge: true })
			.then(() => {
				return 'Document successfully updated';
			})
			.catch((error) => {
				return `Error updating document: ${error}`;
			});

		return result;
	}

	public async updateDocumentProperty(collectionName, documentId, property, newValue): Promise<any> {
		const documentRef = this.firestore.collection(collectionName).doc(documentId);
	
		const updateObject = {};
		updateObject[property] = newValue;
	
		await documentRef.update(updateObject);
	}

	public async getDocumentsByElementInArray(collection: string, id: string, arrayField: string): Promise<any[]> {
		const snapshot = await this.firestore.collection(collection)
			.where(arrayField, 'array-contains', id)
			.get();

		if (snapshot.empty) {
			console.log('No matching documents.');
			return [];
		}

		const documents = [];
		snapshot.forEach(doc => {
			documents.push({ id: doc.id, ...doc.data() });
		});
		return documents;
	}

	
}
