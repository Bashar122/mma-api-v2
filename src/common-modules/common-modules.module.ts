import { Module } from '@nestjs/common';
// import { AuthModule } from './auth/auth.module';
import { HttpModule } from '@nestjs/axios';
import { LoggerModule } from './logger/logger.module';
import { UtilitiesModule } from './utilities/utilities.module';

@Module({
	controllers: [],
	exports: [
		// AuthModule,
		LoggerModule,
		UtilitiesModule,
	],
	imports: [
		// AuthModule,
		LoggerModule,
		UtilitiesModule,
		HttpModule,
	],
	providers: [],
})
export class CommonModulesModule { }
