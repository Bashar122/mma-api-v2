import { Injectable } from '@nestjs/common';
import { AccountService } from '../../endpoints/account/account.service';
import { JwtService } from '@nestjs/jwt';
import { SecurityService } from '../utilities/services/security.service';
import { LoggerService } from '../logger/logger.service';
import { FirestoreService } from '../utilities/services/firestore.service';

@Injectable()
export class AuthService {
	constructor(
		private readonly accountService: AccountService,
		private readonly jwtService: JwtService,
		private readonly loggerService: LoggerService,
		private readonly firestoreService: FirestoreService,
		private readonly security: SecurityService,
	) { }

	// todo: replace all any types
	public async validateUser(email: string, pass: string): Promise<any> {
		/**
		 * Retrieves account associated with email and compares to 
		 * requested password.
		 */
		const account: any = await this.accountService.getUserByEmail(email);
		const isCorrectPassword: boolean = await this.security.compareHash(pass, account.password);

		if (isCorrectPassword) {
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			const { password, ...user } = account;
			return user;
		}

		return null;
	}

	public async login(user: any): Promise<string> {
		return this.jwtService.sign({
			username: user.username,
			sub: user.id
		});
	}

	public async resetPassword(payload: any): Promise<string> {
		const user = await this.accountService.getUserById(payload.id);

		if (this.security.compareHash(payload.password.old, user.password)) {
			try {
				const newPass = await this.security.generateHash(payload.password.new);
				// const result = await this.rethinkDbService.updateDocument(RethinkTables.USERS, user.id, { password: newPass });
				const result = await this.firestoreService.updateDocumentProperty('users', user.id, 'password', newPass);

				if (!result) {
					return 'Error: something went wrong.';
				}

				this.loggerService.verbose(`${user.firstName} ${user.lastName} updated their password.`, '***Password Updated***');
				return 'Password updated.';

			} catch (ex) {
				this.loggerService.error(ex);
				return 'Something went wrong while resetting password.';
			}
		}

		return 'Current password does not match.';
	}
}