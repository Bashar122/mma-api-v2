import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { LoggerService } from 'src/common-modules/logger/logger.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
	constructor(
		private readonly authService: AuthService,
		private readonly logger: LoggerService,
	) {
		super();
	}

	/**
	 * 
	 * @param username - email from user request
	 * @param password - password from user request
	 * 
	 * @function validate( username: string, password: string ) - Passport expects 
	 * this method with this signature for local implemenation
	 */
	public async validate(username: string, password: string): Promise<any> {
		/**
		 * passport intercepts the request, passes email and password to 
		 * auth service to get validated.
		 */
		const user = await this.authService.validateUser(username, password);
		/**
		 * If user does not exist and/or is not validated, throw error
		 */
		if (!user) {
			this.logger.error(`${username} not authorized. Could not authenticate credentials.`);
			throw new UnauthorizedException();
		}

		/**
		 * return user if exists and validated.
		 */
		return user;
	}
}