import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	
	constructor(
		private readonly configService: ConfigService,
	) {
		/**
		 * @super takes in option object to configure passport strategy
		 * 	
		 * @param jwtFromRequest takes in the access token if cookie is present
		 * 						to allow access.
		 * @param ignoreExpiration lets the token be active without a timeframe
		 * @param secretOrKey sign key
		 */
		super({
			jwtFromRequest: ExtractJwt.fromExtractors([
				(req): string => {
					return req?.cookies?.access_token;
				}
			]),
			ignoreExpiration: false,
			secretOrKey: configService.get('JWT'),
		});

	}

	public async validate(payload: any): Promise<any> {
		/**
		 * if authenticated, will return userId and username if present.
		 */
		return { userId: payload.sub, username: payload.username };
	}
}
