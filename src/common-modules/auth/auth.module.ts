import { Module } from '@nestjs/common';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { AccountModule } from '../../endpoints/account/account.module';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategies/jwt.strategy';
import { AuthController } from './auth.controller';
import { CommonModulesModule } from '../common-modules.module';

@Module({
	controllers: [
		AuthController,
	],
	exports: [
		AuthService,
	],
	imports: [
		AccountModule,
		CommonModulesModule,
		JwtModule.registerAsync({
			imports: [ConfigModule],
			useFactory: async (configService: ConfigService) => ({
				secret: configService.get<string>('JWT'),
				useExisting: {
					signOptions: { expiresIn: '60s' },
				}
			}),
			inject: [ConfigService],
		}),
		PassportModule,
	],
	providers: [
		AuthService,
		JwtStrategy,
		LocalStrategy,
	],
})
export class AuthModule { }