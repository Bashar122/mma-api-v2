import { Controller, Get, Request, Response, Post, UseGuards } from '@nestjs/common';
import { LocalAuthGuard } from '../auth/guards/local-auth.guard';
import { AuthService } from '../auth/auth.service';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { SetCookies } from '@nestjsplus/cookies';
import { LoggerService } from '../logger/logger.service';
import { ConfigService } from '@nestjs/config';


@Controller('auth')
export class AuthController {

	constructor(
		private readonly authService: AuthService,
		private readonly configService: ConfigService,
		private readonly logger: LoggerService,
	) { }

	@SetCookies()
	@UseGuards(LocalAuthGuard)
	@Post('login')
	public async login(@Request() request): Promise<any> {
		const jwt = await this.authService.login(request.user);

		request._cookies = [{
			name: 'access_token',
			value: jwt,
			options: {
				signed: false,
				domain: this.configService.get('AUTH_BASE_URL'),
				httpOnly: true,
				//todo: look into how to properly configure secure option
				secure: false,
				sameSite: 'None',
				path: '/'
			},
		}];

		this.logger.verbose(`${request.user.firstName} ${request.user.lastName} has logged in.`, `***User Logged In***`);
		return request.user;
	}

	@UseGuards(JwtAuthGuard)
	@Post('reset-password')
	public async resetPassword(@Request() request): Promise<any> {
		return await this.authService.resetPassword(request.body);
	}

	/**
	 * @function verifyCredentials to check if user is already logged in from client
	 * 
	 * @implements JwtAuthGuard check is access_token is present in header, 
	 * 			   is so, true will be returned
	 */
	@UseGuards(JwtAuthGuard)
	@Get('verify')
	public verifyCredentials(@Response() response, @Request() request): void {
		this.logger.verbose(`${request.user.userId} has been verified.`, `***USER VERIFIED***`);
		response.redirect(`/account/${request.user.userId}`);
	}
}
