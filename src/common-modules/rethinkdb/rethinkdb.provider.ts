import * as r from 'rethinkdb';
import { ConfigService } from '@nestjs/config';

export const RethinkDbProvider = {
	provide: 'RethinkDbProvider',
	useFactory: async (configService: ConfigService): Promise<r.Connection> => {
		const rethinkdbHost = configService.get<string>('RETHINKDB_HOST');
		const rethinkdbPort = configService.get<number>('RETHINKDB_PORT');

		return await r.connect({ host: rethinkdbHost, port: rethinkdbPort });
	},
	inject: [ConfigService],
};
