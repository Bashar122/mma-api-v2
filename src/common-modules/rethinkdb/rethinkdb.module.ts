import { Module } from '@nestjs/common';
import { LoggerModule } from '../logger/logger.module';
import { RethinkDbProvider } from './rethinkdb.provider';
import { RethinkDbService } from './rethinkdb.service';

@Module({
	controllers: [],
	exports: [
		RethinkDbService,
	],
	imports: [
		LoggerModule,
	],
	providers: [
		RethinkDbProvider,
		RethinkDbService,
	],
})
export class RethinkDbModule { }
