import { Injectable, Inject } from '@nestjs/common';
import * as rethink from 'rethinkdb';
import { LoggerService } from '../logger/logger.service';


@Injectable()
export class RethinkDbService {
	private connection: rethink.Connection;

	constructor(
		@Inject('RethinkDbProvider') connection,
		private readonly loggerService: LoggerService,
	) {
		this.connection = connection;
	}

	// TODO: EXAMPLE OF DELETING SPECIFIC PROPERTY IN DOCUMENT
	// r.db('db').table('user').replace(r.row.without('key'))
	//r.db('db').table('user').get('id').replace(r.row.without('key'))
	
	/**
	 * 
	 * @param table table to be queried in db
	 * @param query criteria to use for the query
	 */
	public async getDocument(table: string, query: any = {}): Promise<any> {
		try {
			const cursor: any = await rethink
				.db('fantasy-mma')
				.table(table)
				.filter(query)
				.run(this.connection);

			const result = cursor.next((err, returnedResult) => {
				return returnedResult;
			});
			cursor.close();
	
			return result;
		} catch (ex) {
			this.loggerService.error('RETHINKDB ERROR ON GET DOCUMENT ', ex, query);
		}
	}

	public async getAllDocuments(table: string): Promise<any[]> {
		try {
			const cursor = await rethink
				.db('fantasy-mma')
				.table(table)
				.run(this.connection);
	
			const result = await cursor.toArray();
			cursor.close();
	
			return result;
		} catch (ex) {
			this.loggerService.error('RETHINKDB ERROR ON GET ALL DOCUMENTS ', ex, table);
		}
	}

	public async getDocumentByElementInArray(table: string, query: any = {}): Promise<any> {
		try {
			const cursor: any  = await rethink
				.db('fantasy-mma')
				.table(table)
				.filter(
					rethink
						.row('teams')
						.contains(query)
				)
				.run(this.connection);

			const league = cursor.next((err, returnedResult) => {
				return returnedResult;
			});
			cursor.close();
	
			return league;
		} catch (ex) {
			this.loggerService.error('RETHINKDB ERROR ON getDocumentByElementInArray ', ex, query);
		}
	}

	public async updateDocument(table: string, id: string, updatedDocument: any): Promise<any> {
		try {
			const result: any = await rethink
			.db('fantasy-mma')
			.table(table)
			.filter({
				id: id,
			})
			.update(
			updatedDocument,
			{
				returnChanges: true,
			},
			)
			.run(this.connection);
			
			if (result.changes.length != 0) {
				return result.changes[0].new_val;
			}
				
			return updatedDocument;
		} catch(ex) {
			this.loggerService.error('RETHINKDB ERROR ON UPDATING DOCUMENT ', ex, updatedDocument);
		}
	}

	public async addDocument(table: string, document): Promise<any> {
		try {
			return await rethink
				.db('fantasy-mma')
				.table(table)
				.insert(document)
				.run(this.connection);
		} catch (ex) {
			this.loggerService.error('RETHINKDB ERROR ON ADD DOCUMENT ', ex, document);
		}
	}

	public isConnected(): boolean {
		return this.connection ? true : false;
	}
}
