// import { Injectable } from '@nestjs/common';
// import { RethinkDbService } from 'src/common-modules/rethinkdb/rethinkdb.service';
// import { FirestoreService } from 'src/common-modules/utilities/services/firestore.service';
// import { RethinkTables } from 'src/types/rethinkdb.types';

// @Injectable()
// export class MigrateService {
// 	constructor(
// 		private readonly firestoreService: FirestoreService,
// 		private readonly rethinkDbService: RethinkDbService,
// 	) { }

// 	public async migrateUsers(): Promise<{users: any[]; status: string}> {
// 		const users = await this.rethinkDbService.getAllDocuments(RethinkTables.USERS);

// 		users.forEach(user => {
// 			this.firestoreService.addRecord('users', user);
// 		});

// 		return {
// 			status: 'Users have been migrated to firesbase succesfully!',
// 			users,
// 		};
// 	}

// 	public async migrateTeams(): Promise<{teams: any[]; status: string}> {

// 		console.log('TABLE: ', RethinkTables.TEAMS);
// 		const teams = await this.rethinkDbService.getAllDocuments(RethinkTables.TEAMS);

// 		teams.forEach(team => {
// 			this.firestoreService.addRecord('teams', team);
// 		});

// 		return {
// 			status: 'Teams have been migrated to firesbase succesfully!',
// 			teams,
// 		};
// 	}

// 	public async migrateLeagues(): Promise<{leagues: any[]; status: string}> {
// 		const leagues = await this.rethinkDbService.getAllDocuments(RethinkTables.LEAGUES);
// 		leagues.forEach(league => {
// 			this.firestoreService.addRecord('leagues', league);
// 		});

// 		return {
// 			status: 'Leagues have been migrated to firesbase succesfully!',
// 			leagues,
// 		};
// 	}

// 	public async migrateFighters(): Promise<{fighters: any[]; status: string}> {
// 		const fighters = await this.rethinkDbService.getAllDocuments(RethinkTables.FIGHTERS);
// 		fighters.forEach(fighter => {
// 			this.firestoreService.addRecord('fighters', fighter);
// 		});

// 		return {
// 			status: 'Fighters have been migrated to firesbase succesfully!',
// 			fighters,
// 		};
// 	}

// 	public async migrateEvents(): Promise<{events: any[]; status: string}> {
// 		const events = await this.rethinkDbService.getAllDocuments(RethinkTables.EVENTS);
// 		events.forEach(event => {
// 			this.firestoreService.addRecord('events', event);
// 		});

// 		return {
// 			status: 'Events have been migrated to firesbase succesfully!',
// 			events,
// 		};
// 	}

// 	public async migrateEventsList(): Promise<{eventsList: any[]; status: string}> {
// 		const eventsList = await this.rethinkDbService.getAllDocuments(RethinkTables.EVENT_LISTS);
// 		eventsList.forEach(eventList => {
// 			this.firestoreService.addRecord('events-list', eventList);
// 		});

// 		return {
// 			status: 'Events list have been migrated to firesbase succesfully!',
// 			eventsList,
// 		};
// 	}
// }