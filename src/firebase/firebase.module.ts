import { Module, Global } from '@nestjs/common';
import * as admin from 'firebase-admin';
import { ConfigService, ConfigModule } from '@nestjs/config';
import * as fs from 'fs';

@Global()
@Module({
	imports: [ConfigModule],
	providers: [
		{
			provide: 'FIREBASE_INSTANCE',
			useFactory: (configService: ConfigService): admin.app.App => {
				const serviceAccountPath = configService.get<string>('FIREBASE_SERVICE_ACCOUNT');
				console.log('Service Account Path:', serviceAccountPath);

				if (!serviceAccountPath) {
					throw new Error('FIREBASE_SERVICE_ACCOUNT not found in the environment variables.');
				}

				const serviceAccount = JSON.parse(fs.readFileSync(serviceAccountPath, 'utf8'));

				const firebaseConfig = {
					credential: admin.credential.cert(serviceAccount),
					databaseURL: configService.get<string>('FIREBASE_DATABASE_URL'),
				};

				return admin.initializeApp(firebaseConfig);
			},
			inject: [ConfigService],
		},
	],
	exports: ['FIREBASE_INSTANCE'],
})
export class FirebaseModule {}