import { Module, MiddlewareConsumer } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AccountModule } from './endpoints/account/account.module';
import { HelmetMiddleware } from '@nest-middlewares/helmet';
import { CorsMiddleware } from '@nest-middlewares/cors';
import { LeagueModule } from './endpoints/league/league.module';
import { AuthModule } from './common-modules/auth/auth.module';
import { FirebaseModule } from './firebase';

@Module({
	controllers: [],
	exports: [],
	imports: [
		AccountModule,
		AuthModule,
		ConfigModule.forRoot({
			isGlobal: true,
		}),
		FirebaseModule,
		LeagueModule,
	],
	providers: [
	],
})
export class AppModule {
	constructor(private readonly configService: ConfigService) {}
	/**
	 * @param consumer helper class that runs all middleware
	 */
	public configure(consumer: MiddlewareConsumer): any {
		/**
		 * @module HelmetMiddleware used for http security accross all routes
		 */
		HelmetMiddleware.configure({
			dnsPrefetchControl: true,
			frameguard: true,
			hidePoweredBy: true,
			hsts: true,
			ieNoOpen: true,
			noSniff: true,
			xssFilter: true,
		});

		/**
		 * @module CorsMiddleware used to configure cors rules
		 */
		CorsMiddleware.configure({
			// origin: `https://mma-v1.web.app`,
			origin: `http://localhost:4200`,
			credentials: true,
			allowedHeaders: [
				'Authorization',
				'Content-Type',
				'Accept',
				'Origin',
				'X-Requested-With',
				'Access-Control-Request-Method',
				'Access-Control-Request-Headers',
				'X-CSRF-Token',
				'X-XSRF-Token',
			],
		});

		consumer.apply(CorsMiddleware, HelmetMiddleware).forRoutes('*');
	}
}
