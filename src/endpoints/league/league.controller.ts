import { Controller, Get, Param, Request, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../../common-modules/auth/guards/jwt-auth.guard';
import { LoggerService } from '../../common-modules/logger/logger.service';
import { LeagueService } from './league.service';


@Controller('leagues')
export class LeagueController {

	constructor(
		private readonly leagueService: LeagueService,
		private readonly loggerService: LoggerService,
	) { }

	// @UseGuards(JwtAuthGuard)
	@Get(':id')
	public async getLeagueById(@Param() params): Promise<any> {
		const league = await this.leagueService.getLeagueById(params.id);

		this.loggerService.verbose(`${league.name} retrieved`, `***League Retrieved***`);
		return league;
	}

	// @UseGuards(JwtAuthGuard)
	@Post('add-match-ups')
	public async addMatchUps(@Request() request): Promise<any> {
		if (!request.body.matchups) {
			this.loggerService.error(`Invalid matchups`, null, request.body);
			return 'Can\'t add match-ups, invalid match-ups';
		}

		return await this.leagueService.addMatchUps(request.body);
	}

	// @UseGuards(JwtAuthGuard)
	@Get('containing-team/:teamId')
	public async getLeagueByTeam(@Param() params): Promise<any> {
		const league = await this.leagueService.getLeagueByTeam(params.teamId);

		this.loggerService.verbose(`${league.name} retrieved`, `***League Retrieved***`);
		return league;
	}
	
}
