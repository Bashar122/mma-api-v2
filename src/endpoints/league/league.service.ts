import { Injectable } from '@nestjs/common';
import { TeamsService } from '../teams/teams.service';
import { EventsService } from '../events/events.service';
import { ScoringService } from 'src/common-modules/utilities/services/scoring.service';
import { LoggerService } from 'src/common-modules/logger/logger.service';
import { FirestoreService } from 'src/common-modules/utilities/services/firestore.service';

@Injectable()
export class LeagueService {

	constructor(
		private readonly eventsService: EventsService,
		private readonly scoringService: ScoringService,
		private readonly teamsService: TeamsService,
		private readonly loggerService: LoggerService,
		private readonly firestoreService: FirestoreService,
	) { }

	// Todo: define account object to replace return type of any
	public async getLeagueById(id: string): Promise<any> {
		const league = await this.firestoreService.getRecord('leagues', id);
		league.teams = await this.teamsService.getAllTeams(league.id);

		return league;
	}

	public async getLeagueByTeam(teamId: string): Promise<any> {
		const documents = await this.firestoreService.getDocumentsByElementInArray('leagues', teamId, 'teams');

		if (documents.length === 0) {
			console.log('No matching documents.');
			return [];
		}

		documents[0].teams = await this.teamsService.getAllTeams(documents[0]?.teams);
		documents[0] = await this.checkIfScoreIsOutOfDate(documents[0]);
		
		return documents[0];
	}

	public async addMatchUps({
		matchups, 
		league, 
		event,
		
	}): Promise<any> {
		const teams: Map<string, any> = new Map();
		const eventObject = await this.eventsService.getEvent(event);
		const lineupTotal: number = eventObject?.lineup?.length;
		const week: number = league.teams[0].picks?.length || 0;

		for (const team of league.teams) {
			teams.set(team.id, team);
		}

		matchups?.forEach(async (match) => {
			const team1: any = teams.get(match.team1);
			const team2: any = teams.get(match.team2);

			team1.picks.push({
				opponent: team2.id,
				event: event,
				picks: [],
				result: null,
				totalPoints: 0,
			});

			team2.picks.push({
				opponent: team1.id,
				event: event,
				picks: [],
				result: null,
				totalPoints: 0,
			});

			let index = 0;
			while(lineupTotal > index) {
				team1.picks[week].picks.push({
					winner: null,
					method: null,
					round: null,
					confidence: null,
				});

				team2.picks[week].picks.push({
					winner: null,
					method: null,
					round: null,
					confidence: null,
				});

				index++;
			}

			try {
				await this.firestoreService.updateRecord('teams', team1.id, team1);
				await this.firestoreService.updateRecord('teams', team2.id, team2);
				this.loggerService.verbose(`${team1.teamName} vs ${team2.teamName} match up updated`, `***New Match Up Added***`);
			} catch (ex) {
				this.loggerService.error('Error updating matchup', ex);
				return { team1, team2 , message: 'Error updating matchup'};
			}
		});

		league.teams = league.teams.map((team) => {
			return team.id;
		});

		try {
			return await this.firestoreService.updateRecord('leagues', league.id, league);

		} catch (ex) {
			this.loggerService.error(`Error adding match-ups`, ex);
			return { matchups, message: `Error adding match-ups`};
		}
	}

	private async checkIfScoreIsOutOfDate(league): Promise<any> {
		const arbitraryTeamPicks: any = league.teams[0].picks;
		const currentWeek: number = arbitraryTeamPicks.length - 1;

		if (arbitraryTeamPicks.length === 0) {
			return league;
		}

		const hasEventEnded: boolean = arbitraryTeamPicks[currentWeek]?.result ? true : false;

		// Do not need to update score as all events have been scored and have a result
		if (hasEventEnded) {
			return league;
		}
		
		league.teams = await this.scoringService.scoreMatchUps(league.teams, currentWeek);
		return league;
	}
}