import { Module } from '@nestjs/common';
import { LeagueController } from './league.controller';
import { LeagueService } from './league.service';
import { TeamsModule } from '../teams/teams.module';
import { EventsModule } from '../events/events.module';
import { FightersModule } from '../fighters/fighters.module';
import { CommonModulesModule } from 'src/common-modules/common-modules.module';

@Module({
	controllers: [
		LeagueController,
	],
	exports: [
		EventsModule,
		FightersModule,
		LeagueService,
		TeamsModule,
	],
	imports: [
		CommonModulesModule,
		EventsModule,
		FightersModule,
		TeamsModule,
	],
	providers: [
		LeagueService,
	],
})
export class LeagueModule { }

