import { Controller, Get, Param, Request, UseGuards, Post } from '@nestjs/common';
import { JwtAuthGuard } from 'src/common-modules/auth/guards/jwt-auth.guard';
import { EventsService } from './events.service';
import { LoggerService } from 'src/common-modules/logger/logger.service';

@Controller('events')
export class EventsController {
	constructor(
		private readonly eventsService: EventsService,
		private readonly loggerService: LoggerService,
	) { }

	// @UseGuards(JwtAuthGuard)
	@Get('/get-events-list/:year')
	public async getEventLists(@Param() params): Promise<any> {
		
		if (!params.year) {
			this.loggerService.error('Invalid year', null, params);
			return 'Invalid year';
		}

		const eventList = await this.eventsService.getEventsLists(params.year);
		this.loggerService.verbose(`${params.year} event list retrieved`, `***Event List Retrieved***`);

		return { events: eventList.events };
	}

	// @UseGuards(JwtAuthGuard)
	@Get(':id')
	public async getEvent(@Param() params): Promise<any> {
		if (!params.id) {
			this.loggerService.error('Invalid event id', null, params);
			return 'Invalid event id';
		}

		const event = await this.eventsService.getEvent(params.id);
		this.loggerService.verbose(`${event.eventTitle}`, `***Event Retrieved***`);

		return { event };
	}

	// @UseGuards(JwtAuthGuard)
	@Post('update')
	public async updateEvent(@Request() request): Promise<any> {
		if (!request.body.currentEvent.id) {
			this.loggerService.error(`Invalid event`, null, request.body);
			return 'Can\'t update event, invalid event';
		}

		const updatedEvent = await this.eventsService.updateEvent(request.body.currentEvent, request.body.teams);
		this.loggerService.verbose(`${request.body.eventTitle}`, `***Event Updated***`);

		return { 
			event: updatedEvent.event,
			// message: `${request.body.eventTitle} was updated succesfully` 
			message: updatedEvent.message, 
		};
	}

	// @UseGuards(JwtAuthGuard)
	@Post('new')
	public async createNewEvent(@Request() request): Promise<any> {
		if (!request.body.event.eventTitle) {
			this.loggerService.error(`Invalid event`, null, request.body);
			return 'Can\'t create new event, invalid event';
		}

		const newEvent = await this.eventsService.createNewEvent(request.body.event);

		return newEvent;
	}
}
