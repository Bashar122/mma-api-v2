import { Injectable } from '@nestjs/common';
import { FightersService } from '../fighters/fighters.service';
import { LoggerService } from 'src/common-modules/logger/logger.service';
import { SecurityService } from 'src/common-modules/utilities/services/security.service';
import { TeamValidationService } from 'src/common-modules/utilities/services/team-validation.service';
import { FirestoreService } from 'src/common-modules/utilities/services/firestore.service';

@Injectable()
export class EventsService {
	constructor(
		private readonly fightersService: FightersService,
		private readonly firestoreService: FirestoreService,
		private readonly loggerService: LoggerService,
		private readonly securityService: SecurityService,
		private readonly teamValidationService: TeamValidationService,
	) { }

	public async getEvent(eventId: string): Promise<any> {
		if (!eventId) {
			return;
		}

		let event;

		try {
			event = await this.firestoreService.getRecord('events', eventId);
	
			const updatedLineup = event.lineup.map(async (matchup: any): Promise<any> => {
				matchup.fight.fighter1 = await this.getFighterInEvent(matchup.fight.fighter1Id);
				matchup.fight.fighter2 = await this.getFighterInEvent(matchup.fight.fighter2Id);
				return matchup;
			});
			
			event.lineup = await Promise.all(updatedLineup);
			return event;
		} catch (ex) {
			this.loggerService.error(`Error retrieving ${event?.eventTitle}`, ex);
			return `Error retrieving ${event?.eventTitle}`;
		}
	}

	public async updateEvent(event, teams): Promise<any> {
		try {

			await this.teamValidationService.validatePicks(event, teams);

			return { 
				event: await this.firestoreService.updateRecord('events', event.id, event),
				message: `${event.eventTitle} was updated succesfully`,
			};
		} catch (ex) {
			this.loggerService.error(`Error updating ${event.eventTitle}`, ex);
			return { event, message: `Error updating ${event.eventTitle}`};
		}
	}

	public async createNewEvent(event): Promise<any> {
		try {
			event.id = this.securityService.generateUUID();
			await this.addToEventList(event.id, event.eventTitle);
			this.loggerService.verbose(`${event.id} - ${event.eventTitle} has been created`, `***New Event Created***`);
			return await this.firestoreService.addRecord('events', event);		
		} catch (ex) {
			this.loggerService.error(`Error createing ${event.eventTitle}`, ex);
			return { event, message: `Error updating ${event.eventTitle}`};
		}
	}

	public async getEventsLists(year: string): Promise<any> {
		try {
			const eventList = await this.firestoreService.getRecord('events-list', year);
			this.loggerService.verbose(`${year} event list has been retrieved`, `***New Event List Retrieved***`);
			return eventList;
		} catch (ex) {
			this.loggerService.error(`Error retrieving ${year} event list`, ex);
			return { year, message: `Error retrieving ${year} event list`};
		}
	}

	private async addToEventList(id: string, eventTitle: string): Promise<any> {
		const currentYear = new Date().getFullYear().toString();
		let eventList = await this.getEventsLists(currentYear);

		if (eventList?.message?.indexOf('Error') >= 0) {
			return await this.createNewEventList(id, eventTitle);
		}
		
		try {
			eventList.events.push({
				id,
				eventTitle,
				completed: false,
			});
			
			this.loggerService.verbose(`${eventTitle} added to ${id} event list has been created`, `***New Event Added***`);
			return await this.firestoreService.updateRecord('events-list', currentYear, eventList);
		} catch (ex) {
			this.loggerService.error(`Error updating ${id} event list`, ex);
			return { id, message: `Error updating ${id} event list`};
		}
	}

	private async createNewEventList(id: string, eventTitle: string): Promise<any> {
		try {
			const events = [{
				id,
				eventTitle,
				completed: false,
			}];
			this.loggerService.verbose(`${id} event list has been created`, `***New Event List Created***`);
			return await this.firestoreService.addRecord('events-list', { id: new Date().getFullYear().toString(), events });
		} catch (ex) {
			this.loggerService.error(`Error adding ${id} event list`, ex);
			return { id, message: `Error adding ${id} event list`};
		}
	}

	private async getFighterInEvent(fighterId: string): Promise<any> {
		return await this.fightersService.getFighter(fighterId);
	}
}
