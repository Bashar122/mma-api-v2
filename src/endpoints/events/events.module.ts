import { Module } from '@nestjs/common';
import { EventsService } from './events.service';
import { EventsController } from './events.controller';
import { FightersModule } from '../fighters/fighters.module';
import { CommonModulesModule } from 'src/common-modules/common-modules.module';

@Module({
	controllers: [
		EventsController,
	],
	exports: [
		EventsService,
	],
	imports: [
		CommonModulesModule,
		FightersModule,
	],
	providers: [
		EventsService,
	],
})
export class EventsModule { }
