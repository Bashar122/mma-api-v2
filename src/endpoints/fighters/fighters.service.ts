import { Injectable } from '@nestjs/common';
import { FirestoreService } from 'src/common-modules/utilities/services/firestore.service';

@Injectable()
export class FightersService {
	constructor(
		private readonly firestoreService: FirestoreService,
	) { }

	public async getFighter(fighterId: string): Promise<any> {
		return await this.firestoreService.getRecord('fighters', fighterId);
	}

	public async getAllFighters(): Promise<any> {
		return await this.firestoreService.getAllDocuments('fighters');
	}
}
