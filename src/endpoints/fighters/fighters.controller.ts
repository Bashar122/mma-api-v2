import { Controller, Param, UseGuards, Get, Post, Request } from '@nestjs/common';
import { JwtAuthGuard } from 'src/common-modules/auth/guards/jwt-auth.guard';
import { FightersService } from './fighters.service';
import { LoggerService } from 'src/common-modules/logger/logger.service';
import { FightersHelperService } from 'src/common-modules/utilities/services/fighters-helper.service';

@Controller('fighters')
export class FightersController {
	constructor(
		private readonly fightersService: FightersService,
		private readonly fightersHelpService: FightersHelperService,
		private readonly loggerService: LoggerService,
	) { }

	@Get('allFighters')
	public async getAllFighters(): Promise<any> {
		const allFighters = await this.fightersService.getAllFighters();
		
		return allFighters;
	}

	// @UseGuards(JwtAuthGuard)
	@Get(':id')
	public async getFighter(@Param() params): Promise<any> {
		const fighter = await this.fightersService.getFighter(params.id);

		this.loggerService.verbose(`${fighter.name} retrieved`, `***Fighter Retrieved***`);
		return fighter;
	}

	@Post('getNewFighter')
	public async getNewFighter(@Request() request): Promise<any> {
		if (!request?.body?.path) {
			return { message: 'Invalid path' };
		}

		const fighter = await this.fightersHelpService.getNewFighter(request.body.path);
		
		return fighter; 
	}
}
