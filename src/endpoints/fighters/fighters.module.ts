import { Module } from '@nestjs/common';
import { FightersController } from './fighters.controller';
import { FightersService } from './fighters.service';
import { CommonModulesModule } from 'src/common-modules/common-modules.module';

@Module({
	controllers: [
		FightersController,
	],
	exports: [
		FightersService,
	],
	imports: [
		CommonModulesModule,
	],
	providers: [
		FightersService,
	],
})
export class FightersModule { }
