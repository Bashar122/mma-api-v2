import { Controller, Get, Param, UseGuards, Post, Request } from '@nestjs/common';
import { JwtAuthGuard } from '../../common-modules/auth/guards/jwt-auth.guard';
import { LoggerService } from '../../common-modules/logger/logger.service';
import { AccountService } from './account.service';


@Controller('account')
export class AccountController {

	constructor(
		private readonly accountService: AccountService,
		private readonly logger: LoggerService,
	) { }

	// @UseGuards(JwtAuthGuard)
	@Get(':id')
	public async getUserById(@Param() params): Promise<any> {
		const user = await this.accountService.getUserById(params.id);

		if (!user) {
			return 'Sorry, user not found.';
		}

		this.logger.verbose(`${user.firstName} ${user.lastName} - retrieved account.`, `***User Retrieved***`);
		return user;
	}

	// @UseGuards(JwtAuthGuard)
	@Post('update-password/:id')
	public async updatePassword(@Param() params, @Request() request): Promise<any> {
		if (!params?.id) {
			return { message: 'Invalid user id' };
		}

		const updateMessage = await this.accountService.updatePassword(params.id, request.body.password);
		this.logger.verbose(params.id, `***Password Updated***`);
		return { message: updateMessage };
	}
}
