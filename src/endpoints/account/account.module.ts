import { Module } from '@nestjs/common';
import { AccountService } from './account.service';
import { AccountController } from './account.controller';
import { TeamsModule } from '../teams/teams.module';
import { CommonModulesModule } from 'src/common-modules/common-modules.module';

@Module({
	controllers: [
		AccountController,
	],
	exports: [
		AccountService,
	],
	imports: [
		CommonModulesModule,
		TeamsModule,
	],
	providers: [
		AccountService,
	],
})
export class AccountModule { }
