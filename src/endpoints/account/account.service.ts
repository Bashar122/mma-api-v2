import { Injectable } from '@nestjs/common';
import { TeamsService } from '../teams/teams.service';
import { SecurityService } from 'src/common-modules/utilities/services/security.service';
import { LoggerService } from 'src/common-modules/logger/logger.service';
import { FirestoreService } from 'src/common-modules/utilities/services/firestore.service';

@Injectable()
export class AccountService {

	constructor(
		private readonly firestoreService: FirestoreService,
		private readonly teamsService: TeamsService,
		private readonly security: SecurityService,
		private readonly loggerService: LoggerService,
	) { }

	// Todo: define account object to replace return type of any
	public async getUserByEmail(email: string): Promise<any> {
		const user = await this.firestoreService.getDocumentByField('users', 'email', email);
		user.teams = await this.getUserTeams(user.teams);

		return user;
	}

	// Todo: define account object to replace return type of any
	public async getUserById(id: string): Promise<any> {
		try {
			const user = await this.firestoreService.getRecord('users', id );
			user.teams = await this.getUserTeams(user.teams);
			return user;
		} catch (ex) {
			this.loggerService.error(`Error retrieving user`, id, ex);
		}

	}

	public async updatePassword(id: string, password: string): Promise<string> {
		const hashedPassword = await this.security.generateHash(password);

		try {
			await this.firestoreService.updateRecord('users', id, { password: hashedPassword });
			return 'password updated successfully';
		} catch (ex) {
			this.loggerService.error(`Error updating password`, id, ex);
			return `Error updating password`;
		}
	}

	private async getUserTeams(teams: string[]): Promise<any> {
		const teamsObject = teams.map(async (teamId): Promise<any> => {
			return await this.teamsService.getTeamById(teamId);
		});

		return Promise.all(teamsObject);
	}
}
