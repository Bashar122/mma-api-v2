import { Module } from '@nestjs/common';
import { TeamsController } from './teams.controller';
import { TeamsService } from './teams.service';
import { CommonModulesModule } from 'src/common-modules/common-modules.module';
import { EventsModule } from '../events/events.module';
// import { MigrateService } from 'src/services/migrate.service';
// import { RethinkDbModule } from 'src/common-modules/rethinkdb/rethinkdb.module';

@Module({
	controllers: [
		TeamsController,
	],
	exports: [
		TeamsService,
	],
	imports: [
		CommonModulesModule,
		EventsModule,
		// RethinkDbModule,
	],
	providers: [
		TeamsService,
		// MigrateService,
	],
})
export class TeamsModule { }
