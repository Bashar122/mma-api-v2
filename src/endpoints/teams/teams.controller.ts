import { Controller, Get, Param, UseGuards, Post, Request, Body } from '@nestjs/common';
import { JwtAuthGuard } from 'src/common-modules/auth/guards/jwt-auth.guard';
import { LoggerService } from 'src/common-modules/logger/logger.service';
import { TeamsService } from './teams.service';
import { FirestoreService } from 'src/common-modules/utilities/services/firestore.service';
import { FirebaseAuthGuard } from 'src/guards/firebase/firebase.guard';
// import { MigrateService } from 'src/services/migrate.service';

// TODO make all common modules importable with one module or separate.

@Controller('teams')
export class TeamsController {

	constructor(
		private readonly teamsService: TeamsService,
		private readonly loggerService: LoggerService,
		private readonly firestoreService: FirestoreService,
		// private readonly migrateService: MigrateService,
	) { }

	@Get('')
	public teams(): string {
		return 'LFG HBB!!!!!!!!';
	}

	// @Get('migrate-users')
	// // @UseGuards(FirebaseAuthGuard)
	// public async migrateUsers(): Promise<{ users: any[]; status: string }> {
	// 	// const documentId = await this.firestoreService.addRecord('users', {user: 'test_user', id: '123'});
	// 	const result = await this.migrateService.migrateUsers();
	// 	return result;
	// }

	// @Get('migrate-teams')
	// // @UseGuards(FirebaseAuthGuard)
	// public async migrateTeams(): Promise<{ teams: any[]; status: string }> {
	// 	const result = await this.migrateService.migrateTeams();
	// 	return result;
	// }

	// @Get('migrate-leagues')
	// // @UseGuards(FirebaseAuthGuard)
	// public async migrateLeagues(): Promise<{ leagues: any[]; status: string }> {
	// 	const result = await this.migrateService.migrateLeagues();
	// 	return result;
	// }

	// @Get('migrate-fighters')
	// // @UseGuards(FirebaseAuthGuard)
	// public async migrateFighters(): Promise<{ fighters: any[]; status: string }> {
	// 	const result = await this.migrateService.migrateFighters();
	// 	return result;
	// }

	// @Get('migrate-events')
	// // @UseGuards(FirebaseAuthGuard)
	// public async migrateEvent(): Promise<{ events: any[]; status: string }> {
	// 	const result = await this.migrateService.migrateEvents();
	// 	return result;
	// }

	// @Get('migrate-events-list')
	// // @UseGuards(FirebaseAuthGuard)
	// public async migrateEventList(): Promise<{ eventsList: any[]; status: string }> {
	// 	const result = await this.migrateService.migrateEventsList();
	// 	return result;
	// }

	@Get('get-record/:id')
	public async getRecord(@Param('id') documentId: string): Promise<any> {
		return await this.firestoreService.getRecord('myCollection', documentId);
	}

	// @UseGuards(JwtAuthGuard)
	@Get(':id')
	public async getTeamById(@Param() params): Promise<any> {
		const team = await this.teamsService.getTeamById(params.id);

		this.loggerService.verbose(`${team.name} retrieved`, `***Team Retrieved***`);
		return team;
	}

	// @UseGuards(JwtAuthGuard)
	@Get('all/:leagueId')
	public async getAllTeams(@Param() params): Promise<any> {
		this.loggerService.verbose(`All teams for league: ${params.leagueId} retrieved`, `***All Teams Retrieved***`);
		return await this.teamsService.getAllTeams(params.leagueId);
	}

	// @UseGuards(JwtAuthGuard)
	@Post('update/:id')
	public async updateTeam(@Param() params, @Request() request): Promise<any> {
		const updatedTeam = await this.teamsService.updateTeam(params.id, request.body);

		this.loggerService.verbose(`${updatedTeam.teamName} updated`, `***Team Updated***`);
		return updatedTeam;
	}
}
