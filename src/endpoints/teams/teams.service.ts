import { Injectable } from '@nestjs/common';
import { LoggerService } from 'src/common-modules/logger/logger.service';
import { EventsService } from '../events/events.service';
import { FirestoreService } from 'src/common-modules/utilities/services/firestore.service';

@Injectable()
export class TeamsService {

	constructor(
		private readonly firestoreService: FirestoreService,
		private readonly eventsService: EventsService,
		private readonly loggerService: LoggerService,
	) { }

	// Todo: define account object to replace return type of any
	public async getTeamById(id: string): Promise<any> {
		return await this.firestoreService.getRecord('teams', id);
	}

	public async getAllTeams(teams: any): Promise<any> {
		const allTeams = [];

		for (let teamId of teams) {
			const retrievedTeam = await this.getTeamById(teamId);
			allTeams.push(retrievedTeam);
		}

		return allTeams;
	}

	public async updateTeam(teamId: string, updatedTeam): Promise<any> {
		const currentTeam = await this.firestoreService.getRecord('teams', teamId);
		updatedTeam = await this.checkIfElgibleForUpdate(currentTeam, updatedTeam);

		return this.firestoreService.updateRecord('teams', teamId, updatedTeam);
		
	}

	// This might be removed in the future and not necessary if server side notifications are reliable 
	// at that point the fight will be lock no matter if they refresh or not
	private async checkIfElgibleForUpdate(currentTeam, updatedTeam): Promise<any> {
		const currentWeek: number = updatedTeam.picks.length - 1;
		const eventId: string = updatedTeam.picks[currentWeek].event;
		const event = await this.eventsService.getEvent(eventId);
		
		if (event.completed) {
			return currentTeam;
		}
		
		for (const [index, fight] of event.lineup.entries()) {
			if (fight.started) {
				this.loggerService.error('Match already started, not updating match', null, currentTeam.teamName);
				updatedTeam.picks[currentWeek].picks[index] = currentTeam.picks[currentWeek].picks[index];
			}
		}

		return updatedTeam;
	}
}
