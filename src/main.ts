import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as bodyParser from 'body-parser';


async function bootstrap(): Promise<void> {
	const app = await NestFactory.create(AppModule);
	// const configService = app.get<ConfigService>(ConfigService);
	app.use(bodyParser.json({ limit: '10mb' }));
	app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
	// app.enableCors({
	// 	origin: '*',
	// 	// origin: 'https://mma-v1.web.app',
	// 	// credentials: true,
	// 	allowedHeaders: [
	// 		'Origin',
	// 		'X-Requested-With',
	// 		'Content-Type',
	// 		'Accept',
	// 		'Authorization',
	// 	],
	// 	methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'PATCH', 'HEAD'],
	// 	exposedHeaders: ['Authorization'],
	// 	preflightContinue: false,
	// 	optionsSuccessStatus: 204
	// });

	// await app.listen(configService.get<number>('PORT'), '0.0.0.0');
	const port = process.env.PORT || 3333;
	await app.listen(port, '0.0.0.0');
}
bootstrap();
