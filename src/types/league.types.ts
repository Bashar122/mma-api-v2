export enum ScoringTypes {
	METHOD = 'method',
	CONFIDENCE = 'confidence',
}