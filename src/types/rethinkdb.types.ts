export enum RethinkTables {
	USERS = 'users',
	LEAGUES = 'leagues',
	TEAMS = 'teams',
	EVENTS = 'events',
	FIGHTERS = 'fighters',
	EVENT_LISTS = 'event-lists',
}
