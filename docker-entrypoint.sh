#!/bin/bash

# Start RethinkDB in the background
rethinkdb --bind all &

# Wait for RethinkDB to be available
sleep 5

# Start the NestJS application
npm run start:prod

# which rethinkdb