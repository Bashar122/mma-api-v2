# Use the official Node.js 18 image for ARM64
FROM --platform=linux/amd64 node:18

# Install necessary dependencies for RethinkDB
RUN apt-get update && \
    apt-get install -y wget gnupg && \
    rm -rf /var/lib/apt/lists/*

# Download and install RethinkDB
RUN curl -fsSL https://download.rethinkdb.com/repository/raw/pubkey.gpg | apt-key add - && \
    echo "deb https://download.rethinkdb.com/repository/debian-bookworm bookworm main" > /etc/apt/sources.list.d/rethinkdb.list && \
    apt-get update && \
    apt-get install -y rethinkdb && \
    rm -rf /var/lib/apt/lists/*
    
# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm ci

# Copy the entire project
COPY . .

# Build the NestJS application
RUN npm run build

# Expose the ports for NestJS and RethinkDB
# EXPOSE 3333
EXPOSE 3333 28015 29015 8080

COPY docker-entrypoint.sh /usr/local/bin/

RUN chmod +x /usr/local/bin/docker-entrypoint.sh

CMD ["./docker-entrypoint.sh"]

